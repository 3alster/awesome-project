
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class TestSimulation extends Simulation {

	val uri = "http://example.org"

	val httpProtocol = http
		.baseUrl(uri)
		.inferHtmlResources()
		.disableWarmUp
		.disableCaching

	val headers_8 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Accept-Encoding" -> "gzip, deflate",
		"Accept-Language" -> "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
		"DNT" -> "1",
		"Proxy-Connection" -> "keep-alive",
		"Upgrade-Insecure-Requests" -> "1",
		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4167.0 Safari/537.36")

     

	val scn = scenario("TestSimulation").repeat(10)
	{
		exec(http("TRX_HP")
			.get(uri + "/")
			.headers(headers_8))
		//.pause(7)
		.exec(http("TRX_404")
			.get(uri + "/test")
			.headers(headers_8)
			.check(status.is(404)))
		.exec(flushSessionCookies)
		.exec(flushCookieJar)
		.exec(flushHttpCache)
	}

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}
